function twoEL_01(u1, v1, u2, v2, u3, v3, Ee, nu)
  
   fprintf( '*************************************************************\n');
   fprintf( 'function twoDEL_01( %g, %g, %g, %g, %g, %g, %g, %g )\n', ...
   u1, v1, u2, v2, u3, v3, Ee, nu);
   fprintf( '*************************************************************\n');
   
   % Element geometry  
   x = [ 0; 0.50; 0.5 ];
   y = [ 0; 0.25; 0.5 ];
   
   Ae = polyarea(x,y)
   
   Be = 1/(2*Ae)*[ y(2)-y(3), y(3)-y(1), y(1)-y(2); ...
              x(3)-x(2), x(1)-x(3), x(2)-x(1) ]
             
   %% Determine strains
   d = [ u1, v1; u2, v2; u3, v3 ]*1e-3;

   strain = [ Be(1,:) * d(:,1); Be(2,:) * d(:,2) ] 

   %% Material stiffness matrix
   D = Ee*1e9 / ( (1 + nu) * (1 - 2*nu) ) * [1-nu, nu; nu, 1-nu]
   
   stress = D*strain   
endfunction