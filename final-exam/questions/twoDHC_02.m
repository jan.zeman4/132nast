function twoDHC_02(lambda, alpha, Ta, Tbar)
  
   fprintf( '*************************************************************\n');
   fprintf( 'function twoDHC_02( %g, %g, %g %g )\n', lambda, alpha, Ta, Tbar);
   fprintf( '*************************************************************\n');
   
   % Element geometry  
   x = [ 0; 0.5; 0.0 ];
   y = [ 0; 0.0; 0.5 ];
   
   Ae = polyarea(x,y)

  le = [ norm( [x(2) - x(1); y(2) - y(1) ] ); ... 
          norm( [x(3) - x(2); y(3) - y(2) ] ); ...
          norm( [x(1) - x(3); y(1) - y(3) ] ) ]     
   
   Be = 1/(2*Ae)*[ y(2)-y(3), x(3)-x(2), 
   y(3)-y(1), x(3) - x(10y(1)-y(2); ...
               x(1)-x(3), x(2)-x(1) ]
             
   %% Conductivity conductivity matrix
   K = lambda*Ae*Be'*Be;
   K(2:3,2:3) = K(2:3,2:3) + alpha*le(2)/6*[2,1;1,2]
   
   %% Right hand side matrix
   f = zeros(3,1);
   f(2:3) + alpha*Ta*le(2)/2*[1;1]
    
   %% Apply the essential boundary conditions
   d = [Tbar;Tbar;0];
   E = [1,2];
   F = [3];
   
   disp( 'K(F,F)' ) 
   K(F,F) 
   disp( 'f(F) - K(F,E)*d(E)' ) 
   f(F) - K(F,E)*d(E) 
   
   %% Solve for uknown temperature
   d(F) = K(F,F) \ ( f(F) - K(F,E)*d(E) )   
endfunction