%% Element #1  
x = [ 0; 4; 0 ];
y = [ 4; 4; 0 ];
   
A1 = polyarea(x,y)
B1 = 1/(2*A1)*[ y(2)-y(3), y(3)-y(1), y(1)-y(2); ...           
                x(3)-x(2), x(1)-x(3), x(2)-x(1) ]             
K1 = A1*B1'*B1

%% Element #2
x = [ 4; 4; 0 ];
y = [ 0; 4; 4 ];
   
A2 = polyarea(x,y)
B2 = 1/(2*A2)*[ y(2)-y(3), y(3)-y(1), y(1)-y(2); ...           
                x(3)-x(2), x(1)-x(3), x(2)-x(1) ]
K2 = A2*B2'*B2

%% Assembly 
K = zeros(4, 4);

K( [1,2,3], [1,2,3]) = K1;
K( [2,4,3], [2,4,3]) = K( [2,4,3], [2,4,3]) + K2 

%% Apply the essential boundary conditions
d = [0;0;0;-24];
E = [1,3,4];
F = [2];
      
%% Solve for uknown temperature
d(F) = - K(F,F) \ ( K(F,E)*d(E) )