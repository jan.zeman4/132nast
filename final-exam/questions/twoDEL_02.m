function twoEL_01(u1, v1, u2, v2, u3, v3, Ee, nu)
  
   fprintf( '*************************************************************\n');
   fprintf( 'function twoDEL_01( %g, %g, %g, %g, %g, %g, %g, %g )\n', ...
   u1, v1, u2, v2, u3, v3, Ee, nu);
   fprintf( '*************************************************************\n');
   
   % Element geometry  
   x = [ 0; 0.5; 0.25 ];
   y = [ 0; 0.0; 0.50 ];
   
   Ae = polyarea(x,y)
   
   Be = 1/(2*Ae)*[ x(3)-x(2), y(2)-y(3), x(1)-x(3), y(3)-y(1), x(2)-x(1), ...
   y(1)-y(2) ];
   
   %% Determine strains
   d = [ u1; v1; u2; v2; u3; v3 ]*1e-3;

   strain = Be*d

   %% Material stiffness matrix
   D33 = Ee*1e9 / (2*(1+nu))
   
   stress = D33*strain   
endfunction