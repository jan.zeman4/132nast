function oneDEL_01(l, a, ubar, Ee)
   
   fprintf( '*************************************************************\n');
   fprintf( 'function oneDEL_01( %g, %g, %g, %g )\n', l, a, ubar, Ee );
   fprintf( '*************************************************************\n');
   
   rho = 2500; % Density
   g = 10; % Gravitational acceleration
   
   A = a*a; % Cross-section area
   le = l/2; % Element length 
   b = rho*g*A; % Body force
   
   %% Element 1
   K1 = Ee*1e9*A/le*[1, -1; -1, 1] % Stiffness matrix
   f1 = -b*le/2*[1;1]
   
   %% Element 2
   K2 = K1;
   f2 = f1;
   
   %% Assembly global system
   K = zeros(3, 3);
   f = zeros(3, 1);
   
   K(1:2,1:2) = K1;
   K(2:3,2:3) = K(2:3,2:3) + K2
   
   f(1:2) = f1;
   f(2:3) = f(2:3) + f2
   
   %% Apply the kinematic boundary conditions
   d = [0;0; ubar]*1e-6;
   E = [1;3];
   F = [2];
   
   disp( 'K(F,F)' ) 
   K(F,F) 
   disp( 'f(F) - K(F,E)*d(E)' ) 
   f(F) - K(F,E)*d(E) 
   
   %% Solve for uknown displacement
   d(F) = K(F,F) \ ( f(F) - K(F,E)*d(E) )
   
   %% Determine the stresses
   sig1 = Ee*1e9*(d(2) - d(1))/le
   sig2 = Ee*1e9*(d(3) - d(2))/le   
endfunction