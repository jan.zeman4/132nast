\documentclass[11pt]{article}

\usepackage[a4paper,margin=2cm]{geometry}
\usepackage[utf8]{inputenc}
\usepackage[numbers]{natbib}
\usepackage{graphicx}
\usepackage[colorlinks=true,linkcolor=black,citecolor=blue,urlcolor=blue]{hyperref}
\usepackage{fancyhdr}
\usepackage{pdfpages}
\usepackage{amsmath}
\usepackage{amsfonts}

%% Facyhdr setup
\pagestyle{fancy}%
\fancyhf{}%
\chead{Numerical Analysis of Structures - 132NAST | Instructions for online final exams}%
\rfoot{\thepage}%

\newcommand{\noitemsep}{\itemsep=0pt}

\begin{document}

The final exam will be organized in the \emph{distant mode} because of the current Coronavirus-related restrictions following the most recent instructions for online examination at the Czech Technical University in Prague~\cite[\S II.1.a]{RO-2020/09}. The exam will be held online, simultaneously in the {\bf Moodle} course space~\cite{Moodle} and {\bf MS Teams}~\cite{MSTeam}, according to~\cite[\S II.11]{RO-2020/09}. 

\paragraph{Preliminaries.}

\begin{itemize}\noitemsep
    \item To attend the exam, students must register for the exam in the \href{https://www.kos.cvut.cz/kos/language.do?page}{KOS} university study system. 
    \item By registering for the exam, students explicitly express their agreement with the distant method of examination~\cite[\S II.9]{RO-2020/09}.
    \item By registering for the exam, students further express their consent with \emph{recording} of the test and the oral part of the exam. Moreover, they confirm the availability of the following equipment:
    %
    \begin{itemize}\noitemsep
        \item computer with the necessary software,
        \item web camera,
        \item microphone,
        \item loudspeakers/earphones,
        \item data connection of sufficient quality and capacity,
        \item phone connection as a backup,
    \end{itemize}
    %
    according to~\cite[\S II.10]{RO-2020/09}.

    \item \emph{One day before} the exam, the students will receive via e-mail
    %
    \begin{itemize}\noitemsep
      \item an invitation to the final exam in MS Teams~\cite{MSTeam},
      \item the mobile contact number to reach out to in case of connection problems,
      \item a link for registration for an oral part of the exam in \bf{Google Forms}.  
    \end{itemize}
   
\end{itemize}

\paragraph{Organization of the test component of the exam.}

\begin{itemize}\noitemsep
  \item The student will login into the Moodle course space~\cite{Moodle}, where the test will take place, and MS Teams~\cite{MSTeam}, to perform an identity check~\cite[\S II.17]{RO-2020/09} and to record the test~\cite[\S II.10]{RO-2020/09}.
  
  \item The exam will start at the announced time in MS Team~\cite{MSTeam} with a brief overview of this document. 

  \item Subsequently, each student will turn on his or her camera and mute themself. The camera must remain turned on and students must be visible during the whole test. 
  
  \item Each student will, upon request by the examiner, show her or his ID to the camera.

  \item The test assignment will appear in Moodle~\cite{Moodle} and students can start working on their solutions.
  
  \item After completing the test, students must enter the requested values for each problem into the provided form and submit their solutions via Moodle~\cite{Moodle}.

  \item Moreover, students must scan or take a picture of their tests and attach submit it via Moodle~\cite{Moodle}.

  \item During the whole test, the examiner will be available in MS Teams~\cite{MSTeam} to answer your questions. 
  
  \item The open-book policy applies during the test.

\end{itemize}

\paragraph{Oral part.}

\begin{itemize}\noitemsep
  \item The oral part consists of one-to-one on-line meetings between each student and Jan Zeman, during which they will correct the submitted test and agree on its grading. 
  \item The time allocated for the oral part is \emph{30 minutes} and it will be recorded in MS Teams. 
\end{itemize}

\paragraph{Content.}

\begin{itemize}\noitemsep
  \item The final exam will consist of the following \emph{four problems}:
  
  \begin{enumerate}
    \item One-dimensional elasticity, as covered in Sections~5.1, 5.2, and 5.4 of~\cite{FB07},
    \item One-dimensional heat transfer, as covered in Sections 5.3. and 5.5 of~\cite{FB07},
    \item Two-dimensional heat transfer, as covered in Sections 6.1, 6.2, 6.3, 7.2 (only linear basis functions), and 8.1~(only triangular elements) of~\cite{FB07},  
    \item Two-dimensional elasticity, as covered in Sections 9.1, 9.2, 9.3, and 9.4 of~\cite{FB07}.
  \end{enumerate}

  \item The maximum score for each problem is 10 points.
  \item There is no minimal threshold value for passing the test component of the final exam.
\end{itemize}

\paragraph{Grading policy.}

\begin{itemize}
  \item The evaluation will be based on results for homework~(40 points), mid-term review test (20 points with 50\% threshold), and the final exam test (40 points).
  \item The final grade will be based on the sum of the three criteria according to the following table:
  
  \begin{center}
    \begin{tabular}{ccl}
        \hline
        \bf Sum~$\Sigma$ & \bf Grade & \bf Evaluation \\
        \hline
        $90 \leq \Sigma \leq 100$ & A & Excellent \\
        $80 \leq \Sigma < 90$ & B & Very good \\
        $70 \leq \Sigma < 80$ & C & Good \\
        $60 \leq \Sigma < 70$ & D & Satisfactory \\
        $50 \leq \Sigma < 60$ & E & Sufficient \\
        $0 \leq \Sigma < 50$ & F & Fail \\
        \hline
    \end{tabular}
\end{center}

\item Upon the previous agreement with the student, the final grade will be entered into the KOS system.

\end{itemize}

\begin{thebibliography}{2}\noitemsep

\bibitem{RO-2020/09}
Petráček, V., 2020.
Second complete version of rector's order on the provision of study programs and courses and on the conduct of 
examinations at CTU in Prague, in connection with measures to reduce the risk of 
infection with  the corona virus. URL: 
  \url{https://www.cvut.cz/sites/default/files/content/33d50aa3-800f-4482-9938-bd3f8fff7b95/en/20201226-3-complete-version-of-rectors-order-no-092020.pdf}. [accessed 3 January 2021]

\bibitem{Moodle} B201-132NAST - Numerical Analysis of Structures, \url{https://moodle-vyuka.cvut.cz/course/view.php?id=4497}. [accessed 15 December 2020] 

%\bibitem{MG-2020/02} Achtenová, G., 2020. Methodological guideline by the Vice-Rector for bachelor's and master's studies No. 2/2020
%to hold distance and semi-contact state final exams at CTU. General principles and procedure using Microsoft Teams.
%URL: \url{https://www.cvut.cz/sites/default/files/content/d1dc93cd-5894-4521-b799-c7e715d3c59e/en/20201201-methodical-guideline-no-22020-of-the-vice-rector-for-bachelors-and-masters-studies-for.pdf}. [accessed 15 December 2020]

\bibitem{MSTeam} Team-Course-B201-132NAST, \\ 
URL: \url{https://teams.microsoft.com/l/team/19%3a5cb7283042354c34bf30ed3982640154%40thread.tacv2/conversations?groupId=00864d32-0bea-46d8-b66a-1c77b321acdd&tenantId=f345c406-5268-43b0-b19f-5862fa6833f8}. [accessed 15 December 2020]

\bibitem{FB07} Fish, J. and Belytschko, T. \emph{A first course in finite elements}. John Wiley \& Sons, 2007.
URL: \url{http://dx.doi.org/10.1002/9780470510858}

\end{thebibliography}

\end{document}