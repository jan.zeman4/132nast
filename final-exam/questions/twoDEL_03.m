function twoDEL_03(u1, v1, u2, v2, u3, v3)
  
   fprintf( '*************************************************************\n');
   fprintf( 'function twoDEL_01( %g, %g, %g, %g, %g, %g )\n', ...
   u1, v1, u2, v2, u3, v3);
   fprintf( '*************************************************************\n');
   
   % Element geometry  
   x = [ 0; 0.50; 0.50 ];
   y = [ 0; 0.25; 0.50 ];
   
   Ae = polyarea(x,y)
   
   Be = 1/(2*Ae)*[ x(3)-x(2), y(2)-y(3), x(1)-x(3), y(3)-y(1), x(2)-x(1), ...
   y(1)-y(2) ]
   
   %% Determine strains
   d = [ u1; v1; u2; v2; u3; v3 ]*1e-3

   strain = Be*d
   
endfunction