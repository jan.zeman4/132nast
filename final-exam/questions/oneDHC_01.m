function oneDHC_01(l1, l2, lambda1, lambda2, T1, Ti, alpha)
   
   fprintf( '*************************************************************\n');
   fprintf( 'function oneHC_01( %g, %g, %g, %g, %g, %g, %g )\n', ...
   l1, l2, lambda1, lambda2, T1, Ti, alpha);
   fprintf( '*************************************************************\n');
     
   %% Element 1
   K1 = lambda1/(l1*1e-3)*[1, -1; -1, 1] 
   f1 = [0;0]
   
   %% Element 2
   K2 = lambda2/(l2*1e-3)*[1, -1; -1, 1] + alpha*[0,0;0,1]
   f2 = [0; alpha*Ti]
   
   %% Assembly global system
   K = zeros(3, 3);
   f = zeros(3, 1);
   
   K(1:2,1:2) = K1;
   K(2:3,2:3) = K(2:3,2:3) + K2
   
   f(1:2) = f1;
   f(2:3) = f(2:3) + f2
   
   %% Apply the essential boundary conditions
   d = [T1;0;0];
   E = [1];
   F = [2,3];
   
   disp( 'K(F,F)' ) 
   K(F,F) 
   disp( 'f(F) - K(F,E)*d(E)' ) 
   f(F) - K(F,E)*d(E) 
   
   %% Solve for uknown temperature
   d(F) = K(F,F) \ ( f(F) - K(F,E)*d(E) )   
endfunction